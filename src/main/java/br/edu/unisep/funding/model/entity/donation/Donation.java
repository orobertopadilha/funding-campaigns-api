package br.edu.unisep.funding.model.entity.donation;

import br.edu.unisep.funding.model.entity.campaign.Campaign;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "donations")
public class Donation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "donation_id")
    private Integer id;

    @Column(name = "use_id")
    private Integer user;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "date")
    private LocalDate date;

    @OneToOne
    @JoinColumn(name = "campaign_id")
    private Campaign campaign;

}
