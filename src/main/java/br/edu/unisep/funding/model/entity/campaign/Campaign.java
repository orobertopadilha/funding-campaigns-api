package br.edu.unisep.funding.model.entity.campaign;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "campaigns")
public class Campaign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "campaign_id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "min_donation")
    private Double minDonation;

    @Column(name = "max_donation")
    private Double maxDonation;

    @Column(name = "creation_date")
    private LocalDate creationDate;

}
