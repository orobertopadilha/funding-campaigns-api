package br.edu.unisep.funding.model.repository;

import br.edu.unisep.funding.model.entity.donation.Donation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DonationRepository extends JpaRepository<Donation, Integer> {

    @Query
    List<Donation> findByUser(Integer user);

}
