package br.edu.unisep.funding.controller.campaign;

import br.edu.unisep.funding.domain.dto.campaign.CampaignDto;
import br.edu.unisep.funding.domain.usecase.campaign.FindAllCampaignsUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/campaigns")
public class CampaignController {

    private final FindAllCampaignsUseCase findAllCampaigns;

    @GetMapping
    public ResponseEntity<List<CampaignDto>> findAll() {
        var campaigns = findAllCampaigns.execute();
        return ResponseEntity.ok(campaigns);
    }

}
