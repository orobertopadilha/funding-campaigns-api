package br.edu.unisep.funding.controller.donation;

import br.edu.unisep.funding.domain.dto.donation.DonationDto;
import br.edu.unisep.funding.domain.dto.donation.RegisterDonationDto;
import br.edu.unisep.funding.domain.usecase.donation.FindDonationsByUserUseCase;
import br.edu.unisep.funding.domain.usecase.donation.RegisterDonationUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/donations")
public class DonationController {

    private final FindDonationsByUserUseCase findDonationsByUser;
    private final RegisterDonationUseCase registerDonation;

    @GetMapping
    public ResponseEntity<List<DonationDto>> find(@RequestHeader("funding-user-id") Integer user) {
        var donations = findDonationsByUser.execute(user);
        return ResponseEntity.ok(donations);
    }

    @PostMapping
    public ResponseEntity save(@RequestBody RegisterDonationDto donation, @RequestHeader("funding-user-id") Integer user) {
        registerDonation.execute(donation, user);
        return ResponseEntity.ok().build();
    }

}
