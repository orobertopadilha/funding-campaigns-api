package br.edu.unisep.funding.domain.builder;

import br.edu.unisep.funding.domain.dto.donation.DonationDto;
import br.edu.unisep.funding.domain.dto.donation.RegisterDonationDto;
import br.edu.unisep.funding.model.entity.campaign.Campaign;
import br.edu.unisep.funding.model.entity.donation.Donation;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DonationBuilder {

    public DonationDto from(Donation donation) {
        return new DonationDto(donation.getId(),
                donation.getAmount(),
                donation.getDate(),
                donation.getCampaign().getTitle());
    }

    public List<DonationDto> from(List<Donation> donations) {
        return donations.stream().map(this::from).collect(Collectors.toList());
    }

    public Donation from(RegisterDonationDto donation, Integer user) {
        var newDonation = new Donation();
        newDonation.setAmount(donation.getAmount());
        newDonation.setCampaign(new Campaign());
        newDonation.getCampaign().setId(donation.getCampaign());
        newDonation.setDate(LocalDate.now());
        newDonation.setUser(user);
        return newDonation;
    }
}
