package br.edu.unisep.funding.domain.builder;

import br.edu.unisep.funding.domain.dto.campaign.CampaignDto;
import br.edu.unisep.funding.model.entity.campaign.Campaign;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CampaignBuilder {

    public CampaignDto from(Campaign campaign) {
        return new CampaignDto(campaign.getId(),
                campaign.getTitle(),
                campaign.getDescription(),
                campaign.getMinDonation(),
                campaign.getMaxDonation(),
                campaign.getCreationDate());
    }

    public List<CampaignDto> from(List<Campaign> campaigns) {
        return campaigns.stream().map(this::from).collect(Collectors.toList());
    }

}
