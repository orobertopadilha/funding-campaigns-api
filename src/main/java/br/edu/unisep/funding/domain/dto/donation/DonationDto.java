package br.edu.unisep.funding.domain.dto.donation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class DonationDto {

    private Integer id;

    private Double amount;

    private LocalDate date;

    private String campaign;
}
