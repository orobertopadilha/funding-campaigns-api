package br.edu.unisep.funding.domain.dto.campaign;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class CampaignDto {

    private Integer id;

    private String title;

    private String description;

    private Double minDonation;
    private Double maxDonation;

    private LocalDate creationDate;
}
