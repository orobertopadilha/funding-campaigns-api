package br.edu.unisep.funding.domain.usecase.donation;

import br.edu.unisep.funding.domain.builder.DonationBuilder;
import br.edu.unisep.funding.domain.dto.donation.RegisterDonationDto;
import br.edu.unisep.funding.model.repository.DonationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegisterDonationUseCase {

    private final DonationRepository repository;
    private final DonationBuilder builder;

    public void execute(RegisterDonationDto donation, Integer user) {
        var newDonation = builder.from(donation, user);
        repository.save(newDonation);
    }
}
