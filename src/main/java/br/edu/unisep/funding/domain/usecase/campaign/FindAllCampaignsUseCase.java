package br.edu.unisep.funding.domain.usecase.campaign;

import br.edu.unisep.funding.domain.builder.CampaignBuilder;
import br.edu.unisep.funding.domain.dto.campaign.CampaignDto;
import br.edu.unisep.funding.model.repository.CampaignRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllCampaignsUseCase {

    private final CampaignRepository repository;
    private final CampaignBuilder builder;

    public List<CampaignDto> execute() {
        var campaigns = repository.findAll(Sort.by(Sort.Order.asc("date")));
        return builder.from(campaigns);
    }

}
