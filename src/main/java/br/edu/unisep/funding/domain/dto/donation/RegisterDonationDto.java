package br.edu.unisep.funding.domain.dto.donation;

import lombok.Data;

@Data
public class RegisterDonationDto {

    private Double amount;
    private Integer campaign;
}
