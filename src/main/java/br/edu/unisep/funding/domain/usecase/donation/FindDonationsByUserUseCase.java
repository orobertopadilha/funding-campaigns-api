package br.edu.unisep.funding.domain.usecase.donation;

import br.edu.unisep.funding.domain.builder.DonationBuilder;
import br.edu.unisep.funding.domain.dto.donation.DonationDto;
import br.edu.unisep.funding.model.repository.DonationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindDonationsByUserUseCase {

    private final DonationRepository repository;
    private final DonationBuilder builder;

    public List<DonationDto> execute(Integer user) {
        var donations = repository.findByUser(user);
        return builder.from(donations);
    }

}
