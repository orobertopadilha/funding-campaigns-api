package br.edu.unisep.funding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CampaignsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CampaignsApiApplication.class, args);
	}

}
